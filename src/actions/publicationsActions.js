import axios from 'axios';
import { ERROR,
    UPDATED,
    LOADING,
    LOADING_COM,
    ERROR_COM,
    UPDATED_COM
} from "../types/publicationsTypes";
import * as usersTypes from '../types/UsersTypes';

const { GET_ALL: GET_ALL_USER } = usersTypes;

export const getByUser = (key) => async (dispatch, getState) => {
    const { users } = getState().usersReducer;
    const { publications } = getState().publicationsReducer;
    const id = users[key].id;
    dispatch({
        type: LOADING
    });
    try {
        const response = await axios.get(`http://jsonplaceholder.typicode.com/posts?userId=${id}`);
        const newPosts = response.data.map((publication) =>({
            ...publication,
            comments : [],
            open: false

        }));
        const publications_updated = [
            ...publications,
            newPosts
        ];
        dispatch({
            type: UPDATED,
            payload: publications_updated
        });

        const publications_key = publications_updated.length - 1;
        const users_updated = [...users];

        users_updated[key] = {
            ...users[key],
            publications_key // publications_key:publications_key
        };
        dispatch({
            type: GET_ALL_USER,
            payload: users_updated
        });
    } catch (e) {
        console.log(e.message);
        dispatch({
            type: ERROR,
            payload: "Publicaciones no disponibles"
        });
    }
};

export const openAndClose = (pubKey, comKey) => (dispatch, getState) => {
    const { publications } = getState().publicationsReducer;
    const selected = publications[pubKey][comKey];

    const updated = {
        ...selected,
        open : !selected.open
        };

    const updatedPosts = [ ...publications ];

    updatedPosts[pubKey] = [
        ...publications[pubKey]
    ];

    updatedPosts[pubKey][comKey] = updated;

    dispatch({
        type: UPDATED,
        payload: updatedPosts
    });
};

export const getComments = (pubKey, comKey) => async (dispatch, getState) => {
    //Obtego publicaciones del estado
    const { publications } = getState().publicationsReducer;
    //Obtengo la publicacion seleccionada
    const selected = publications[pubKey][comKey];

    dispatch({
        type: LOADING_COM
    });

    try {

        const response = await axios.get(`http://jsonplaceholder.typicode.com/comments?postId=${selected.id}`);

        const updated = {
            ...selected,
            comments : response.data
        };

        const updatedPosts = [ ...publications ];

        updatedPosts[pubKey] = [
            ...publications[pubKey]
        ];

        updatedPosts[pubKey][comKey] = updated;

        dispatch({
            type: UPDATED_COM,
            payload: updatedPosts
        });
    } catch (e) {
        dispatch({
            type: ERROR_COM,
            payload: "Comentarios no disponibles"
        });
    }
};
