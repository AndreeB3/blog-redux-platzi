import axios from 'axios';
import {ERROR, GET_ALL, LOADING} from "../types/UsersTypes";

export const getAll = () => async (dispatch) => {
    dispatch({
        type: LOADING
    });
    try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/users');
        dispatch({
            type: GET_ALL,
            payload: response.data
        });
    } catch (error) {
        console.log('Error: ', error.message);
        dispatch({
            type: ERROR,
            payload: 'Informacion de usuario no disponible'
        })
    }
};

