import React from 'react';

const Error = (props) => (
    <h2 className="center red"> {props.message}</h2>
);

export default Error;