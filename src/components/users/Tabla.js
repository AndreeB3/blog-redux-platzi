import React from "react";
import { connect } from 'react-redux';
import "./../../css/icons.css";
import { Link } from "react-router-dom";

const Tabla = (props) => {

    const buildRows = () => props.users.map((user,i) => (
            <tr key={user.id}>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.website}</td>
                <td>
                    <Link to={`/publications/${i}`}>
                        <div className="eye icon"> </div>
                    </Link>
                </td>
            </tr>
    ));

    return (
        <div>
            <table className="table">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Web</th>
                </tr>
                </thead>
                <tbody>
                {
                    buildRows()
                }
                </tbody>
            </table>
        </div>
    )
};

const mapStateToProps =  (reducers) => {
    return reducers.usersReducer;
};

export default connect(mapStateToProps)(Tabla);