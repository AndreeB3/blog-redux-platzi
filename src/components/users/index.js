import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as usersActions from '../../actions/usersActions'
import Spinner from "../commons/Spinner";
import Error from "../commons/Error"
import Tabla from "./Tabla";

class Users extends Component {

    componentDidMount() {
        if(!this.props.users.length) {
            this.props.getAll();
        }

    }
    putContent = () =>{
        if (this.props.loading) {
            return ( <Spinner/> )
        }
        if (this.props.error) {
            return ( <Error message={ this.props.error } />)
        }
        return (
            <Tabla />
        )
    } ;

    render() {
        return (
            <div >
                <h1> Usuarios </h1>
                {this.putContent()}
            </div>
        )
    }
}

const mapStateToProps = (reducers) => {
    return reducers.usersReducer;
};
export default connect(mapStateToProps, usersActions)(Users)
