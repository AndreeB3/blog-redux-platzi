import React from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import Menu from './Menu';
import Users from "./users";
import Publications from "./publications";


const Works = () => (<div> Tareas </div>);

const App = () => (
    <BrowserRouter>
        <Menu />
        <div className="div_margin">
            <Route exact path='/' component={Users} />
            <Route exact path='/works' component={Works} />
            <Route exact path='/publications/:key' component={Publications} />
        </div>
    </BrowserRouter>

);
export default  App;