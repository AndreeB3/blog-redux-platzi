import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as usersActions from '../../actions/usersActions';
import * as publicationsActions from '../../actions/publicationsActions';
import Spinner from '../commons/Spinner';
import Error from "../commons/Error";
import Comments from "./Comments";
const { getAll : getAllUsers } =  usersActions;
const {
    getByUser : getPublicationsByUser,
    openAndClose,
    getComments } = publicationsActions;


class Publications extends Component {

    async componentDidMount() {
        const {
            getAllUsers,
            getPublicationsByUser,
            match: {
                params: { key }
            }
        } = this.props;
        if (!this.props.usersReducer.users.length) {
            await getAllUsers();
        }
        if (this.props.usersReducer.error) {
            return;
        }
        if (!('publications_key' in this.props.usersReducer.users[key])) {
            getPublicationsByUser(key);
        }

    }

    putUser = () => {
        const {
            usersReducer,
            match: {
                params: { key }
            }
        } = this.props;
        if ( usersReducer.error) {
            return <Error message={ usersReducer.error}/>
        }
        if (!usersReducer.users.length || usersReducer.loading) {
            return <Spinner/>
        }
        const name = usersReducer.users[key].name;
        return  (
            <h1>Publicaciones de {name} </h1>
        )
    };

    putPublications = () => {
        const {
            usersReducer,
            usersReducer: {users},
            publicationsReducer,
            publicationsReducer: { publications },
            match: {
                params: {key}
            }
        } = this.props;
        if (!users.length) return;
        if (usersReducer.error) return;
        if (publicationsReducer.loading) {
            return <Spinner/>
        }
        if (publicationsReducer.error) {
            return <Error message={publicationsReducer.error}/>
        }
        if (!publications.length) return;

        if (!('publications_key' in users[key])) return;

        const { publications_key } = users[key];
        return this.showInfo(publications[publications_key], publications_key);

    };

    showInfo = (posts, postKey) => (
        posts.map((publication, comKey) => (
            <div  className="pub-title"
                  key={publication.id}
                  onClick={ () => this.showComments(postKey, comKey, publication.comments)} >
                <h2>
                    { publication.title }
                </h2>
                <h3>
                    { publication.body }
                </h3>
                {
                    publication.open ? <Comments comments={ publication.comments } /> : ''
                }
            </div>
        ))
    );

    showComments = (postKey, comKey, comments) => {
        this.props.openAndClose(postKey, comKey);
        if (!comments.length) {
            this.props.getComments(postKey, comKey,);
        }
    };

    render() {
        return (
            <div>
                { this.putUser() }
                { this.putPublications() }
            </div>
        )
    }
}
const mapStateToProps = ({ usersReducer , publicationsReducer}) => {
    return {
        usersReducer,
        publicationsReducer
    };
};

const mapDispatchToProps = {
    getAllUsers,
    getPublicationsByUser,
    openAndClose,
    getComments

};

export default connect(mapStateToProps, mapDispatchToProps)(Publications);
