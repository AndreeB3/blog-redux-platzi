import React from 'react';
import Spinner from '../commons/Spinner';
import Error from '../commons/Error';
import { connect } from 'react-redux';

const Comments = (props) => {

    if (props.error_com) {
        return <Error message={props.error_com}/>
    }
    if (props.loading_com && !props.comments.length) {
        return <Spinner/>
    }
    const putComments = () => (
      props.comments.map((comment) => (
          <li>
              <b>
                  <u>
                      { comment.email }
                  </u>
              </b>
              <br/>
              { comment.body }
          </li>
          )
      )
    );

    return (
        <ul>
            { putComments() }
        </ul>
    )
};

const mapStateToProps = ({ publicationsReducer }) => publicationsReducer;

export default connect(mapStateToProps)(Comments);