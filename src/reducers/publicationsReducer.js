import {
    UPDATED,
    ERROR,
    LOADING,
    LOADING_COM,
    ERROR_COM,
    UPDATED_COM
} from "../types/publicationsTypes";

const INITIAL_STATE = {
    publications: [],
    loading: false,
    error: '',
    loading_com: false,
    error_com: ''

};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case UPDATED:
            return { ...state, publications: action.payload, loading: false, error: ''};
        case LOADING:
            return { ...state, loading: true};
        case ERROR:
            return { ...state, error: action.payload, loading: false};
        case LOADING_COM:
            return { ...state, loading_com: true};
        case ERROR_COM:
            return { ...state, error_com: action.payload, loading: false};
        case UPDATED_COM:
            return { ...state, publications: action.payload, loading_com: false, error_com: ''};
        default:
            return state;

    }
}