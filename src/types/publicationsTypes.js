export const UPDATED = 'update_publications';
export const LOADING = 'loading_publications';
export const ERROR = 'error_publications';
export const ERROR_COM = 'error_comments';
export const LOADING_COM = 'loading_comments';
export const UPDATED_COM = 'update_comments';
